package com.exaltcompany.infrastructure.entity;

public enum TransactionType {
    DEPOSIT("D"),
    WITHDRAWAL("W");

    String type;

    TransactionType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
