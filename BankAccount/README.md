# Bank Account Kata

## User Stories
```
In order to implement this Kata, think of your personal bank account experience.
When in doubt, go for the simplest solution Requirements

* Deposit and Withdrawal
* Account statement (date, amount, balance)
* Statement printing
 

## User Story 1

In order to save money

As a bank client

I want to make a deposit in my account


## User Story 2

In order to retrieve some or all of my savings

As a bank client

I want to make a withdrawal from my account


## User Story 3

In order to check my operations

As a bank client

I want to see the history (operation, date, transactionAmount, balance) of my operations
```

## API

Base url : http://localhost:8080/api/v1/bank
- Find all clients : GET /client
- Get account statement : GET/client/{idClient}/account/{idAccount}
- Deposit : POST /client/{idClient}/account/{idAccount}/transaction/deposit
- Withdrawal : POST /client/{idClient}/account/{idAccount}/transaction/withdrawal

The only client available has the id 1 and his account number is 12397237.

## Run

- Unix : run.sh
- Windows : run.bat
