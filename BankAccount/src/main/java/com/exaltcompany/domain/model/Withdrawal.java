package com.exaltcompany.domain.model;

import java.time.LocalDateTime;

/**
 * Withdrawal
 *
 * @author pyref 08/08/2022
 */
public class Withdrawal extends Transaction {
    public Withdrawal(double amount, LocalDateTime transactionDate, double previousBalanceAmount, double newBalanceAmount, Account account) {
        super(amount, transactionDate, previousBalanceAmount, newBalanceAmount, account);
    }

    public Withdrawal() {
    }
}
