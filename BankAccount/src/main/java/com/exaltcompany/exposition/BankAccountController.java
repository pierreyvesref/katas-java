package com.exaltcompany.exposition;

import com.exaltcompany.domain.exception.UnauthorizedTransaction;
import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.Client;
import com.exaltcompany.domain.model.Transaction;
import com.exaltcompany.domain.port.ClientService;
import com.exaltcompany.domain.port.DepositService;
import com.exaltcompany.domain.port.PrintService;
import com.exaltcompany.domain.port.WithdrawalService;
import com.exaltcompany.exposition.dto.TransactionAmount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/bank")
public class BankAccountController {

    private ClientService clientService;

    private DepositService depositService;

    private WithdrawalService withdrawalService;

    private PrintService printService;

    @Autowired
    public BankAccountController(ClientService clientService, DepositService depositService,
                                 WithdrawalService withdrawalService, PrintService printService) {
        this.clientService = clientService;
        this.depositService = depositService;
        this.withdrawalService = withdrawalService;
        this.printService = printService;
    }

    @GetMapping(value = "/client")
    public ResponseEntity<List<Client>> getClients() {
        return new ResponseEntity<>(clientService.findAllClients(), HttpStatus.OK);
    }

    @GetMapping(value = "/client/{idClient}/account/{idAccount}")
    public ResponseEntity<String> getAccountStatement(@PathVariable long idClient, @PathVariable long idAccount){
        String accountHistory;
        try {
            accountHistory = printService.printAccountStatement(idClient, idAccount);
        } catch (UnknownRessourceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(accountHistory, HttpStatus.OK);
    }

    @PostMapping(value = "/client/{idClient}/account/{idAccount}/transaction/deposit")
    public ResponseEntity<String> deposit(@PathVariable long idClient, @PathVariable long idAccount, @RequestBody TransactionAmount transactionAmount){
        Transaction transaction;
        try {
            transaction = depositService.depositInAccount(idClient, idAccount, transactionAmount.getAmount());
        } catch (UnknownRessourceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (UnauthorizedTransaction e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(transaction.toString(), HttpStatus.OK);
    }

    @PostMapping(value = "/client/{idClient}/account/{idAccount}/transaction/withdrawal")
    public ResponseEntity<String> withdraw(@PathVariable long idClient, @PathVariable long idAccount, @RequestBody TransactionAmount transactionAmount){
        Transaction transaction;
        try {
            transaction = withdrawalService.withdrawFromAccount(idClient, idAccount, transactionAmount.getAmount());
        } catch (UnknownRessourceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (UnauthorizedTransaction e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }
        return new ResponseEntity<>(transaction.toString(), HttpStatus.OK);
    }

}
