package com.exalt.company;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Test class for StringCalculatorKata
 *
 * @author pyref 07/08/2022
 */
class StringCalculatorKataTest {

    private StringCalculatorKata stringCalculatorKata;

    @BeforeEach
    public void init(){
        stringCalculatorKata = new StringCalculatorKata();
    }

    static Stream<Arguments> stringNumbersToAdd() {
        return Stream.of(
                Arguments.of("", 0),
                Arguments.of("1", 1),
                Arguments.of("1,2", 3),
                Arguments.of("1,2,5", 8),
                Arguments.of("1\n2,3", 6),
                Arguments.of("//;\n1;2", 3)
        );
    }

    @DisplayName("Add numbers test")
    @ParameterizedTest
    @MethodSource("stringNumbersToAdd")
    void addTest(String stringNumbers, int expected){
        int result = stringCalculatorKata.add(stringNumbers);
        Assertions.assertThat(result).isEqualTo(expected);
    }

    static Stream<Arguments> stringNotOnlyNumbers() {
        return Stream.of(
                Arguments.of("2,a"),
                Arguments.of("1,\n"),
                Arguments.of("1,4.5")
        );
    }

    @DisplayName("Add not only numbers test")
    @ParameterizedTest
    @MethodSource("stringNotOnlyNumbers")
    void addTestNotNumberKo(String stringNumbers){
        Assertions.assertThatThrownBy(() -> stringCalculatorKata.add(stringNumbers))
                .isInstanceOf(NumberFormatException.class)
                .hasMessage("string does not contain only numbers");
    }


    static Stream<Arguments> stringNegativeNumbers() {
        return Stream.of(
                Arguments.of("-1,2,5", "negatives not allowed : -1"),
                Arguments.of("1\n2,-5,3,-4", "negatives not allowed : -5 -4"),
                Arguments.of("//;\n-99;2;-76;4\n-5", "negatives not allowed : -99 -76 -5")
        );
    }

    @DisplayName("Add negative numbers test")
    @ParameterizedTest
    @MethodSource("stringNegativeNumbers")
    void addTestNegativeNumberKo(String stringNumbers, String exceptionMessageExpected){
        Assertions.assertThatThrownBy(() -> stringCalculatorKata.add(stringNumbers))
                .isInstanceOf(NumberFormatException.class)
                .hasMessage(exceptionMessageExpected);
    }

}