package com.exaltcompany.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Table(name = "ACCOUNT")
public class AccountEntity implements Serializable {

    private static final long serialVersionUID = 1234567L;

    @Id
    @Column(unique = true, nullable = false)
    private long accountNumber;

    @Column(name = "balance", nullable = false)
    private double balance;

    @Column(name = "creation_date", columnDefinition = "DATE", nullable = false)
    private LocalDate creationDate;

    @Column(name = "closing_date", columnDefinition = "DATE")
    private LocalDate closingDate;

    @Column(name = "minimum_balance")
    private double minimumBalance;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "client_id")
    private ClientEntity client;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<TransactionEntity> transactions;

}
