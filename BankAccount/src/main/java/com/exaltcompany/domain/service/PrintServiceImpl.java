package com.exaltcompany.domain.service;

import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.Account;
import com.exaltcompany.domain.model.Client;
import com.exaltcompany.domain.model.Transaction;
import com.exaltcompany.domain.port.AccountRepositoryPort;
import com.exaltcompany.domain.port.ClientRepositoryPort;
import com.exaltcompany.domain.port.PrintService;
import com.exaltcompany.domain.port.TransactionRepositoryPort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PrintServiceImpl implements PrintService {

    private ClientRepositoryPort clientRepositoryPort;

    private AccountRepositoryPort accountRepositoryPort;

    private TransactionRepositoryPort transactionRepositoryPort;

    public PrintServiceImpl(ClientRepositoryPort clientRepositoryPort, AccountRepositoryPort accountRepositoryPort,
                            TransactionRepositoryPort transactionRepositoryPort) {
        this.clientRepositoryPort = clientRepositoryPort;
        this.accountRepositoryPort = accountRepositoryPort;
        this.transactionRepositoryPort = transactionRepositoryPort;
    }

    @Override
    public String printAccountStatement(long clientId, long accountNumber) throws UnknownRessourceException {

        Client client = clientRepositoryPort.findClientById(clientId).orElseThrow(()-> new UnknownRessourceException("Unknown client"));
        Account account = accountRepositoryPort.findAccountByAccountNumber(accountNumber).orElseThrow(()-> new UnknownRessourceException("Unknown account"));
        List<Transaction> transactions = transactionRepositoryPort.findTransactionsByAccountNumber(account.getAccountNumber());
        account.setTransactions(transactions);

        return stringBuilder(client, account);

    }

    public String stringBuilder(Client client, Account account) {

        String newLine = "\n";

        return new StringBuilder()
                .append("History of account n° " + account.getAccountNumber() + " : ")
                .append(newLine)
                .append(newLine)
                .append("Owner : " + client.getFistName() + " " + client.getLastName().toUpperCase())
                .append(newLine)
                .append("Creation date : " +account.getCreationDate())
                .append(newLine)
                .append("Closing date : " + (account.getClosingDate() != null ? account.getClosingDate() : "not closed"))
                .append(newLine)
                .append("Minimum balance authorized : " + account.getMinimumBalance())
                .append(newLine)
                .append("Current Balance : " + account.getBalance())
                .append(newLine)
                .append(newLine)
                .append("Transactions : ")
                .append(newLine)
                .append(account.getTransactions().stream().map(Transaction::toString).collect(Collectors.joining("\n")))
                .toString();
    }

}
