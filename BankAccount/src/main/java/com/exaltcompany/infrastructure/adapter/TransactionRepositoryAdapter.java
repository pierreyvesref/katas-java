package com.exaltcompany.infrastructure.adapter;

import com.exaltcompany.domain.model.Transaction;
import com.exaltcompany.domain.port.TransactionRepositoryPort;
import com.exaltcompany.infrastructure.entity.TransactionEntity;
import com.exaltcompany.infrastructure.map.EntityModelMapper;
import com.exaltcompany.infrastructure.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@Transactional
public class TransactionRepositoryAdapter implements TransactionRepositoryPort {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private EntityModelMapper entityModelMapper;

    @Override
    public void saveTransaction(Transaction transaction) {
        TransactionEntity transactionEntity = entityModelMapper.mapTransactionToTransactionEntity(transaction);
        transactionRepository.save(transactionEntity);
    }

    @Override
    public List<Transaction> findTransactionsByAccountNumber(long accountNumber) {
        List<TransactionEntity> transactionEntityList = transactionRepository.findTransactionsByAccountNumber(accountNumber);
        return transactionEntityList.stream().map(entityModelMapper::mapTransactionEntityToTransaction).collect(Collectors.toList());
    }
}
