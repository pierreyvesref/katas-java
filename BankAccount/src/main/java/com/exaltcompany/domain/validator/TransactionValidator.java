package com.exaltcompany.domain.validator;

import com.exaltcompany.domain.model.Account;
import com.exaltcompany.domain.model.Transaction;
import org.springframework.stereotype.Service;

@Service
public class TransactionValidator {
    public boolean validateTransactionAmount(double amount) {
        return (amount > 0);
    }

    public boolean validateTransactionDate(Account account, Transaction transaction) {

        boolean transactionDateValid = true;

        if(account.getClosingDate() != null){
            transactionDateValid = transaction.getTransactionDate().isBefore(account.getClosingDate().atStartOfDay());
        }

        return transactionDateValid;
    }

    public boolean validateTransactionFundCapacity(Account account, Transaction transaction) {
        return transaction.getNewBalanceAmount() >= account.getMinimumBalance();
    }
}
