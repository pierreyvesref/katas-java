package com.exalt.company.validator;

import java.util.regex.Pattern;

/**
 * NumberValidator
 *
 * @author pyref 07/08/2022
 */
public class NumberValidator {

    /* patternNumeric - Pattern, pattern to validate a number */
    private static final Pattern patternNumeric = Pattern.compile("-?\\d+");

    /**
     * Validate that a string is a number
     *
     * @param stringNumber string number to validate
     * @return boolean stringNumber is a number or not
     */
    public boolean isInteger(String stringNumber) {
        return patternNumeric.matcher(stringNumber).matches();
    }

    /**
     * Validate that a stringNumber is positive
     *
     * @param stringNumber number to validate
     * @return boolean number is positive or not
     */
    public boolean isPositive(String stringNumber) {
        return Integer.parseInt(stringNumber) >= 0;
    }


}
