package com.exaltcompany.domain.port;

import com.exaltcompany.domain.model.Transaction;

import java.util.List;

/**
 * TransactionRepositoryPort - port for transaction persistence
 *
 * @author pyref 08/08/2022
 */
public interface TransactionRepositoryPort {

    /**
     * Save a transaction
     *
     * @param transaction transaction to save
     */
    void saveTransaction(Transaction transaction);

    /**
     * Find a transactiobn by its account
     *
     * @param accountNumber number of the account of the transactions to find
     * @return Transaction object
     */
    List<Transaction> findTransactionsByAccountNumber(long accountNumber);
}
