package com.exaltcompany.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Table(name = "TRANSACTION")
public class TransactionEntity implements Serializable {

    private static final long serialVersionUID = 1238999L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(name = "amount", nullable = false)
    private double amount;

    @Column(name = "transaction_date", columnDefinition = "TIMESTAMP", nullable = false)
    private LocalDateTime transactionDate;

    @Column(name = "previous_balance_amount", nullable = false)
    private double previousBalanceAmount;

    @Column(name = "new_balance_amount", nullable = false)
    private double newBalanceAmount;

    @Column(name = "transactionType", nullable = false)
    private String transactionType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_number_id")
    private AccountEntity account;

}
