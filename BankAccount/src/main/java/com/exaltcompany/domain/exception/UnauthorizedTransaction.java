package com.exaltcompany.domain.exception;

public class UnauthorizedTransaction extends Exception {
    public UnauthorizedTransaction(String message) {
        super(message);
    }
}
