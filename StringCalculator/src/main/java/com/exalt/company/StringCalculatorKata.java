package com.exalt.company;

import com.exalt.company.validator.NumberValidator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * StringCalculatorKata
 *
 * @author pyref 07/08/2022
 */
public class StringCalculatorKata {

    /* NumberValidator */
    NumberValidator numberValidator;

    /* delimiterRegex - String, delimiter */
    private String delimiterRegex = ",";

    public StringCalculatorKata() {
        this.numberValidator = new NumberValidator();
    }

    /**
     * Add all the numbers of a string seperating by comma (up to 2 numbers)
     *
     * @param numbers string containing numbers to add, seperating by comma
     * @return addition between the numbers of the string
     */
    public int add(String numbers) {

        if (numbers.isEmpty()) {
            return 0;
        }

        else{

            /* Manage a new delimiter */
            if(numbers.startsWith("//")){
                String[] strToDelimit = numbers.split("\n", 2);
                delimiterRegex = strToDelimit[0].substring(2);
                numbers = strToDelimit[1];
            }

            /* Replace new lines by the current delimiter */
            numbers = numbers.replace("\n", delimiterRegex);

            // For a reason split(String) does not include empty elements (ex : "1,,2" gives the same array than "1,2")
            List<String> numberList = Arrays.asList(numbers.split(delimiterRegex, Integer.MAX_VALUE));

            /* test if string list contains others characters than numbers */
            boolean containsNotOnlyNumber = numberList.stream().anyMatch(s -> !numberValidator.isInteger(s));

            if (containsNotOnlyNumber) {
                throw new NumberFormatException("string does not contain only numbers");
            }

            /* test if string list contains negative numbers */
            String negativeNumbers = numberList.stream()
                    .filter(s -> !numberValidator.isPositive(s))
                    .collect(Collectors.joining(" "));

            if(!negativeNumbers.isEmpty()){
                throw new NumberFormatException("negatives not allowed : " + negativeNumbers);
            }

            return numberList.stream()
                    .mapToInt(Integer::parseInt)
                    .sum();
        }

    }
}
