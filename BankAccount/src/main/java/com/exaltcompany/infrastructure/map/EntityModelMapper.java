package com.exaltcompany.infrastructure.map;

import com.exaltcompany.domain.model.*;
import com.exaltcompany.infrastructure.entity.AccountEntity;
import com.exaltcompany.infrastructure.entity.ClientEntity;
import com.exaltcompany.infrastructure.entity.TransactionEntity;
import com.exaltcompany.infrastructure.entity.TransactionType;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class EntityModelMapper {

    public Client mapClientEntityToClient(ClientEntity clientEntity){
        Client client = new Client();
        client.setId(clientEntity.getId());
        client.setLastName(clientEntity.getLastName());
        client.setFistName(clientEntity.getFistName());
        client.setBirthDate(clientEntity.getBirthDate());
        client.setAdress(clientEntity.getAdress());
        client.setPostalCode(clientEntity.getPostalCode());

        return client;
    }

    public ClientEntity mapClientToClientEntity(Client client){
        ClientEntity clientEntity = new ClientEntity();
        clientEntity.setId(client.getId());
        clientEntity.setLastName(client.getLastName());
        clientEntity.setFistName(client.getFistName());
        clientEntity.setBirthDate(client.getBirthDate());
        clientEntity.setAdress(client.getAdress());
        clientEntity.setPostalCode(client.getPostalCode());

        return clientEntity;
    }

    public Account mapAccountEntityToAccount(AccountEntity accountEntity){
        Account account = new Account();
        account.setAccountNumber(accountEntity.getAccountNumber());
        account.setCreationDate(accountEntity.getCreationDate());
        account.setClosingDate(accountEntity.getClosingDate());
        account.setMinimumBalance(accountEntity.getMinimumBalance());
        account.setBalance(accountEntity.getBalance());
        account.setTransactions(accountEntity.getTransactions().stream()
                .map(this::mapTransactionEntityToTransaction)
                .collect(Collectors.toList()));
        account.setClient(mapClientEntityToClient(accountEntity.getClient()));

        return account;
    }

    public AccountEntity mapAccountToAccountEntity(Account account) {
        AccountEntity accountEntity = new AccountEntity();
        accountEntity.setAccountNumber(account.getAccountNumber());
        accountEntity.setCreationDate(account.getCreationDate());
        accountEntity.setClosingDate(account.getClosingDate());
        accountEntity.setMinimumBalance(account.getMinimumBalance());
        accountEntity.setBalance(account.getBalance());
        accountEntity.setClient(mapClientToClientEntity(account.getClient()));

        return accountEntity;
    }

    public TransactionEntity mapTransactionToTransactionEntity(Transaction transaction) {
        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setId(transaction.getId());
        transactionEntity.setTransactionType(transaction instanceof Deposit ? TransactionType.DEPOSIT.getType() : TransactionType.WITHDRAWAL.getType());
        transactionEntity.setTransactionDate(transaction.getTransactionDate());
        transactionEntity.setAmount(transaction.getAmount());
        transactionEntity.setPreviousBalanceAmount(transaction.getPreviousBalanceAmount());
        transactionEntity.setNewBalanceAmount(transaction.getNewBalanceAmount());
        transactionEntity.setAccount(mapAccountToAccountEntity(transaction.getAccount()));
        return transactionEntity;
    }

    public Transaction mapTransactionEntityToTransaction(TransactionEntity transactionEntity) {
        Transaction transaction;

        if(transactionEntity.getTransactionType().equals(TransactionType.DEPOSIT.getType())){
            transaction = new Deposit();
        }
        else {
            transaction = new Withdrawal();
        }

        transaction.setId(transactionEntity.getId());
        transaction.setTransactionDate(transactionEntity.getTransactionDate());
        transaction.setAmount(transactionEntity.getAmount());
        transaction.setPreviousBalanceAmount(transactionEntity.getPreviousBalanceAmount());
        transaction.setNewBalanceAmount(transactionEntity.getNewBalanceAmount());

        return transaction;
    }
}
