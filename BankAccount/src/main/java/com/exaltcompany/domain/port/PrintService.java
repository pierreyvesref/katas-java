package com.exaltcompany.domain.port;

import com.exaltcompany.domain.exception.UnauthorizedTransaction;
import com.exaltcompany.domain.exception.UnknownRessourceException;

/**
 * HistoryService - Service which deals with print operations
 *
 * @author pyref 09/08/2022
 */
public interface PrintService {

    /**
     *
     * @param clientId id of the client who deposit
     * @param accountNumber id of the account to deposit in
     * @return deposit processed
     * @throws UnknownRessourceException
     * @throws UnauthorizedTransaction
     */
    String printAccountStatement(long clientId, long accountNumber) throws UnknownRessourceException;

}
