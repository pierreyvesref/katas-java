package com.exaltcompany.domain.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Transaction
 *
 * @author pyref 08/08/2022
 */
public abstract class Transaction {

    /* id - long, id of the transaction */
    private long id;

    /* amount - double, amount of the transaction */
    private double amount;

    /* transactionDate - LocalDateTime, date of the transaction */
    private LocalDateTime transactionDate;

    /* previousBalanceAmount - double, previous amount of the account balance */
    private double previousBalanceAmount;

    /* newBalanceAmount - double, new amount of the account balance */
    private double newBalanceAmount;

    private Account account;

    protected Transaction(){}

    protected Transaction(double amount, LocalDateTime transactionDate, double previousBalanceAmount, double newBalanceAmount, Account account) {
        this.amount = amount;
        this.transactionDate = transactionDate;
        this.previousBalanceAmount = previousBalanceAmount;
        this.newBalanceAmount = newBalanceAmount;
        this.account = account;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public double getPreviousBalanceAmount() {
        return previousBalanceAmount;
    }

    public void setPreviousBalanceAmount(double previousBalanceAmount) {
        this.previousBalanceAmount = previousBalanceAmount;
    }

    public double getNewBalanceAmount() {
        return newBalanceAmount;
    }

    public void setNewBalanceAmount(double newBalanceAmount) {
        this.newBalanceAmount = newBalanceAmount;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "    " + transactionDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + " : " +
                "Amount = " + amount + "  -  " +
                "Previous balance = " + previousBalanceAmount + "  -  " +
                "New Balance=" + newBalanceAmount;
    }
}
