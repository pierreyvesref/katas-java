package com.exaltcompany.infrastructure.adapter;

import com.exaltcompany.domain.model.Account;
import com.exaltcompany.domain.port.AccountRepositoryPort;
import com.exaltcompany.infrastructure.entity.AccountEntity;
import com.exaltcompany.infrastructure.map.EntityModelMapper;
import com.exaltcompany.infrastructure.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AccountRepositoryAdapter implements AccountRepositoryPort {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private EntityModelMapper entityModelMapper;

    @Override
    public Optional<Account> findAccountByAccountNumber(long accountNumber) {
        Optional<AccountEntity> accountEntity = accountRepository.findById(accountNumber);
        return Optional.of(entityModelMapper.mapAccountEntityToAccount(accountEntity.get()));
    }

    @Override
    public void saveAccount(Account account) {
        AccountEntity accountEntity = entityModelMapper.mapAccountToAccountEntity(account);
        accountRepository.save(accountEntity);
    }
}
