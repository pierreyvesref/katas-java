package com.exaltcompany.domain.port;

import com.exaltcompany.domain.exception.UnauthorizedTransaction;
import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.Transaction;

/**
 * DepositService - Service which deals with deposit operations
 *
 * @author pyref 08/08/2022
 */
public interface DepositService {

    /**
     *
     * @param clientId id of the client who deposit
     * @param accountNumber id of the account to deposit in
     * @param amount amount to deposit
     * @return deposit processed
     * @throws UnknownRessourceException
     * @throws UnauthorizedTransaction
     */
    Transaction depositInAccount(long clientId, long accountNumber, double amount) throws UnknownRessourceException, UnauthorizedTransaction;
}
