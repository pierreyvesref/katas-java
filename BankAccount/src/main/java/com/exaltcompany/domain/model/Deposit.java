package com.exaltcompany.domain.model;

import java.time.LocalDateTime;

/**
 * Deposit
 *
 * @author pyref 08/08/2022
 */
public class Deposit extends Transaction {
    public Deposit(double amount, LocalDateTime transactionDate, double previousBalanceAmount, double newBalanceAmount, Account account) {
        super(amount, transactionDate, previousBalanceAmount, newBalanceAmount, account);
    }

    public Deposit() {
        super();
    }
}
