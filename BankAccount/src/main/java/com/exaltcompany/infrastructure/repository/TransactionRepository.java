package com.exaltcompany.infrastructure.repository;

import com.exaltcompany.infrastructure.entity.TransactionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionEntity, Long> {

    @Query(value = "SELECT a.transactions " +
            "FROM AccountEntity a " +
            "WHERE a.accountNumber = ?1"
    )
    List<TransactionEntity> findTransactionsByAccountNumber(long accountNumber);

}
