package com.exaltcompany.domain;

import com.exaltcompany.domain.model.Account;
import com.exaltcompany.domain.model.Client;
import com.exaltcompany.domain.model.Deposit;
import com.exaltcompany.domain.model.Transaction;
import com.exaltcompany.domain.validator.TransactionValidator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Stream;

/**
 * Test class for AccountService
 *
 * @author pyref 08/08/2022
 */
public class TransactionValidatorTest {

    TransactionValidator transactionValidator;

    @BeforeEach
    public void init(){
        transactionValidator = new TransactionValidator();
    }

    static Stream<Arguments> amounts() {
        return Stream.of(
                Arguments.of(1, true),
                Arguments.of(200, true),
                Arguments.of(24379374, true),
                Arguments.of(234.23, true),
                Arguments.of(0.54, true),
                Arguments.of(0, false),
                Arguments.of(-45, false),
                Arguments.of(-0.10, false),
                Arguments.of(-423.43, false),
                Arguments.of(-34.2093, false)
        );
    }

    @DisplayName("Validate amount test")
    @ParameterizedTest
    @MethodSource("amounts")
    public void validateTransactionAmountTest(double amount, boolean expected) {
        Assertions.assertThat(transactionValidator.validateTransactionAmount(amount)).isEqualTo(expected);
    }

    @Test
    public void validateTransactionDateTest() {
        Account account = new Account(1837240, 1000.00, LocalDate.of(2020, 7, 5),LocalDate.of(2022, 1, 1), -1000, new Client());
        Transaction transaction = new Deposit(200.00, LocalDateTime.of(2022, 6, 1, 1, 1),1000.00, 1200.00, account);
        Assertions.assertThat(transactionValidator.validateTransactionDate(account, transaction)).isFalse();
    }

    static Stream<Arguments> amountsForWithdrawal() {
        return Stream.of(
                Arguments.of(50, true),
                Arguments.of(1000, true),
                Arguments.of(2000, false)
        );
    }

    @DisplayName("Validate withdrawal according to maximum payment capacity")
    @ParameterizedTest
    @MethodSource("amountsForWithdrawal")
    public void validateFundCapacityTest(double amount, boolean expected) {
        double balance = 0;
        double minBalance = -1000.00;
        Account account = new Account(1837240, balance, LocalDate.of(2020, 7, 5),LocalDate.of(2022, 1, 1), minBalance, new Client());
        Transaction transaction = new Deposit(amount, LocalDateTime.of(2022, 6, 1, 1, 1),balance, balance - amount, account);
        Assertions.assertThat(transactionValidator.validateTransactionFundCapacity(account, transaction)).isEqualTo(expected);
    }

}
