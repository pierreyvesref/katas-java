package com.exaltcompany.domain;

import com.exaltcompany.domain.exception.UnauthorizedTransaction;
import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.*;
import com.exaltcompany.domain.port.AccountRepositoryPort;
import com.exaltcompany.domain.port.ClientRepositoryPort;
import com.exaltcompany.domain.port.TransactionRepositoryPort;
import com.exaltcompany.domain.service.TransactionServiceImpl;
import com.exaltcompany.domain.validator.TransactionValidator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Test class for AccountService
 *
 * @author pyref 08/08/2022
 */
public class TransactionServiceTest {

    TransactionServiceImpl accountService;

    ClientRepositoryPort clientRepositoryPort;

    AccountRepositoryPort accountRepositoryPort;

    TransactionRepositoryPort transactionRepositoryPort;

    TransactionValidator transactionValidator;

    @BeforeEach
    public void init(){
        clientRepositoryPort = Mockito.mock(ClientRepositoryPort.class);
        accountRepositoryPort = Mockito.mock(AccountRepositoryPort.class);
        transactionRepositoryPort = Mockito.mock(TransactionRepositoryPort.class);
        transactionValidator = Mockito.mock(TransactionValidator.class);
        accountService = new TransactionServiceImpl(clientRepositoryPort, accountRepositoryPort, transactionRepositoryPort, transactionValidator);
    }

    @Test
    public void depositTest() throws UnknownRessourceException, UnauthorizedTransaction {

        double amount = 50.00;

        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        client.setAccounts(List.of(account));
        account.setClient(client);

        when(clientRepositoryPort.findClientById(client.getId())).thenReturn(Optional.of(client));
        when(accountRepositoryPort.findAccountByAccountNumber(account.getAccountNumber())).thenReturn(Optional.of(account));
        when(transactionValidator.validateTransactionAmount(amount)).thenReturn(true);
        when(transactionValidator.validateTransactionDate(Mockito.any(Account.class), Mockito.any(Transaction.class))).thenReturn(true);
        doNothing().when(transactionRepositoryPort).saveTransaction(Mockito.isA(Transaction.class));
        doNothing().when(accountRepositoryPort).saveAccount(Mockito.isA(Account.class));

        Transaction deposit = accountService.depositInAccount(client.getId(), account.getAccountNumber(), amount);

        Assertions.assertThat(account.getBalance()).isEqualTo(1050.00);

        Assertions.assertThat(deposit.getAmount()).isEqualTo(50.00);
        Assertions.assertThat(deposit.getPreviousBalanceAmount()).isEqualTo(1000.00);
        Assertions.assertThat(deposit.getNewBalanceAmount()).isEqualTo(1050.00);
    }
    @Test
    public void withdrawTest() throws UnknownRessourceException, UnauthorizedTransaction {

        double amount = 50.00;

        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        client.setAccounts(List.of(account));
        account.setClient(client);

        when(clientRepositoryPort.findClientById(client.getId())).thenReturn(Optional.of(client));
        when(accountRepositoryPort.findAccountByAccountNumber(account.getAccountNumber())).thenReturn(Optional.of(account));
        when(transactionValidator.validateTransactionAmount(amount)).thenReturn(true);
        when(transactionValidator.validateTransactionDate(Mockito.any(Account.class), Mockito.any(Withdrawal.class))).thenReturn(true);
        when(transactionValidator.validateTransactionFundCapacity(Mockito.any(Account.class), Mockito.any(Transaction.class))).thenReturn(true);
        doNothing().when(transactionRepositoryPort).saveTransaction(Mockito.isA(Transaction.class));
        doNothing().when(accountRepositoryPort).saveAccount(Mockito.isA(Account.class));

        Transaction deposit = accountService.withdrawFromAccount(client.getId(), account.getAccountNumber(), amount);

        Assertions.assertThat(account.getBalance()).isEqualTo(950.00);

        Assertions.assertThat(deposit.getAmount()).isEqualTo(50.00);
        Assertions.assertThat(deposit.getPreviousBalanceAmount()).isEqualTo(1000.00);
        Assertions.assertThat(deposit.getNewBalanceAmount()).isEqualTo(950.00);
    }


    @Test
    public void transactionTestUnknownClient() {

        double amount = 50.00;

        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        client.setAccounts(List.of(account));

        when(clientRepositoryPort.findClientById(client.getId())).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> accountService.depositInAccount(client.getId(), account.getAccountNumber(), amount))
                .isInstanceOf(UnknownRessourceException.class)
                .hasMessage("Unknown client");
    }

    @Test
    public void transactionTestUnknownAccount() {

        double amount = 50.00;

        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        client.setAccounts(List.of(account));

        when(clientRepositoryPort.findClientById(client.getId())).thenReturn(Optional.of(client));
        when(accountRepositoryPort.findAccountByAccountNumber(account.getAccountNumber())).thenReturn(Optional.empty());

        Assertions.assertThatThrownBy(() -> accountService.depositInAccount(client.getId(), account.getAccountNumber(), amount))
                .isInstanceOf(UnknownRessourceException.class)
                .hasMessage("Unknown account");
    }

    @Test
    public void validateTransactionTestClientNotOwnerOfAccount() {

        Client clientOwner = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        account.setClient(clientOwner);

        Client clientNotOwner = new Client();
        clientNotOwner.setId(2L);

        Assertions.assertThatThrownBy(() -> accountService.validateTransaction(clientNotOwner, account, Mockito.mock(Transaction.class)))
                .isInstanceOf(UnauthorizedTransaction.class)
                .hasMessage("Client not owner of this account");
    }

    @Test
    public void validateTransactionTestBadAmount() {

        double amount = 34.64;

        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        client.setAccounts(List.of(account));
        account.setClient(client);

        when(transactionValidator.validateTransactionAmount(amount)).thenReturn(false);

        Assertions.assertThatThrownBy(() -> accountService.validateTransaction(client, account, Mockito.mock(Transaction.class)))
                .isInstanceOf(UnauthorizedTransaction.class)
                .hasMessage("Bad amount");
    }

    @Test
    public void validateTransactionTestClosedAccount() {

        double amount = 34.64;

        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), LocalDate.of(2022, 1, 1), -1000, client);
        account.setClosingDate(LocalDate.of(2022, 1, 1));
        client.setAccounts(List.of(account));

        Transaction transaction = new Deposit(amount, LocalDateTime.now(), 1000.00, 1034.63, account);

        when(transactionValidator.validateTransactionAmount(amount)).thenReturn(true);
        when(transactionValidator.validateTransactionDate(account, transaction)).thenReturn(false);

        Assertions.assertThatThrownBy(() -> accountService.validateTransaction(client, account, transaction))
                .isInstanceOf(UnauthorizedTransaction.class)
                .hasMessage("Account is closed");
    }

    @Test
    public void validateFundCapacityTestNotEnoughFunds() {

        double amount = 5000;

        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), LocalDate.of(2022, 1, 1), -1000, new Client());
        account.setClosingDate(LocalDate.of(2022, 1, 1));

        Transaction transaction = new Deposit(amount, LocalDateTime.now(), 1000.00, -4000, account);

        when(transactionValidator.validateTransactionFundCapacity(account, transaction)).thenReturn(false);

        Assertions.assertThatThrownBy(() -> accountService.validateFundCapacity(account, transaction))
                .isInstanceOf(UnauthorizedTransaction.class)
                .hasMessage("Account has not enough funds");
    }

}
