package com.exaltcompany.domain.model;

import java.time.LocalDate;
import java.util.List;

/**
 * Account
 *
 * @author pyref 08/08/2022
 */
public class Account {

    /* accountNumber - long, number of the account */
    private long accountNumber;

    /* balance - double, balance amount in account */
    private double balance;

    /* creationDate - LocalDate, date of creation of the account */

    private LocalDate creationDate;

    /* closingDate - LocalDate, date of closing of the account */
    private LocalDate closingDate;

    /* minimumBalance - double, stop limit in balance */
    private double minimumBalance;

    /* transactions - List<Transaction>, list of transactions of the account */
    List<Transaction> transactions;

    Client client;

    public Account(){}

    public Account(long accountNumber, double balance, LocalDate creationDate, double minimumBalance) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.creationDate = creationDate;
        this.minimumBalance = minimumBalance;
    }

    public Account(long accountNumber, double balance, LocalDate creationDate, LocalDate closingDate, double minimumBalance , Client client) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.creationDate = creationDate;
        this.closingDate = closingDate;
        this.minimumBalance = minimumBalance;
        this.client = client;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double addToBalance(double amount){
        this.balance += amount;
        return this.balance;
    }

    public double substractToBalance(double amount){
        this.balance -= amount;
        return this.balance;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDate getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(LocalDate closingDate) {
        this.closingDate = closingDate;
    }

    public double getMinimumBalance() {
        return minimumBalance;
    }

    public void setMinimumBalance(double minimumBalance) {
        this.minimumBalance = minimumBalance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void addTransaction(Transaction transaction){
        transactions.add(transaction);
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
