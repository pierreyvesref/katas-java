package com.exaltcompany.domain.service;

import com.exaltcompany.domain.model.Client;
import com.exaltcompany.domain.port.ClientRepositoryPort;
import com.exaltcompany.domain.port.ClientService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    private ClientRepositoryPort clientRepositoryPort;

    public ClientServiceImpl(ClientRepositoryPort clientRepositoryPort) {
        this.clientRepositoryPort = clientRepositoryPort;
    }

    @Override
    public List<Client> findAllClients() {
        return clientRepositoryPort.findAllClients();
    }

    @Override
    public Client findClientById(long idClient) {
        return null;
    }
}
