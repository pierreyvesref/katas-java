DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS account;

CREATE TABLE client (
    id              LONG AUTO_INCREMENT PRIMARY KEY,
    last_name       VARCHAR(32) NOT NULL,
    first_name      VARCHAR(32) NOT NULL,
    birth_date      DATE NOT NULL,
    adress          VARCHAR(64) NOT NULL,
    postal_code     VARCHAR(16) NOT NULL
);

CREATE TABLE account (
    account_number       LONG PRIMARY KEY,
    balance             DECIMAL(10,2) NOT NULL,
    creation_date        VARCHAR(32) NOT NULL,
    closing_date         DATE,
    minimum_balance      VARCHAR(250) NOT NULL,
    client_id           LONG
);

ALTER TABLE account
    ADD FOREIGN KEY (client_id) REFERENCES client(id);


CREATE TABLE transaction (
    id                      LONG AUTO_INCREMENT PRIMARY KEY,
    amount                  DECIMAL(10,2) NOT NULL,
    transaction_type        VARCHAR(1) NOT NULL,
    transaction_date         DATETIME NOT NULL,
    previous_balance_amount   DECIMAL(10,2) NOT NULL,
    new_balance_amount        DECIMAL(10,2) NOT NULL,
    account_number_id       LONG
);

ALTER TABLE transaction
    ADD FOREIGN KEY (account_number_id) REFERENCES account(account_number);