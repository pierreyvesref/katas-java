package com.exaltcompany.domain;

import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.*;
import com.exaltcompany.domain.port.AccountRepositoryPort;
import com.exaltcompany.domain.port.ClientRepositoryPort;
import com.exaltcompany.domain.port.PrintService;
import com.exaltcompany.domain.port.TransactionRepositoryPort;
import com.exaltcompany.domain.service.PrintServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.when;

public class PrintServiceTest {

    PrintService printService;

    ClientRepositoryPort clientRepositoryPort;

    AccountRepositoryPort accountRepositoryPort;

    TransactionRepositoryPort transactionRepositoryPort;

    @BeforeEach
    public void init(){
        clientRepositoryPort = Mockito.mock(ClientRepositoryPort.class);
        accountRepositoryPort = Mockito.mock(AccountRepositoryPort.class);
        transactionRepositoryPort = Mockito.mock(TransactionRepositoryPort.class);
        printService = new PrintServiceImpl(clientRepositoryPort, accountRepositoryPort, transactionRepositoryPort);
    }

    @Test
    public void printAccountStatementTest() throws UnknownRessourceException {
        Client client = new Client(1L, "Dupont", "Guillaume", LocalDate.of(1997, 1, 1),"9 rue de la Tour", "92330");
        Account account = new Account(2746493279L, 1000.00, LocalDate.of(2020, 7, 5), -1000);
        client.setAccounts(List.of(account));
        Transaction t1 = new Deposit(500.00, LocalDateTime.of(2021, Month.APRIL.getValue(), 1, 20, 45), 1000, 1500, account);
        Transaction t2 = new Withdrawal(700.00, LocalDateTime.of(2022, Month.JANUARY.getValue(), 4, 18, 0), 1500, 800, account);
        Transaction t3 = new Withdrawal(50.00, LocalDateTime.of(2022, Month.FEBRUARY.getValue(), 23, 5, 30), 800, 750, account);
        Transaction t4 = new Deposit(250.00, LocalDateTime.of(2022, Month.MAY.getValue(), 22, 15, 0), 750, 1000, account);
        account.setTransactions(List.of(t1, t2, t3, t4));

        when(clientRepositoryPort.findClientById(client.getId())).thenReturn(Optional.of(client));
        when(accountRepositoryPort.findAccountByAccountNumber(account.getAccountNumber())).thenReturn(Optional.of(account));
        when(transactionRepositoryPort.findTransactionsByAccountNumber(account.getAccountNumber())).thenReturn(List.of(t1, t2, t3, t4));

        String result = printService.printAccountStatement(client.getId(), account.getAccountNumber());

        String accountHistory = stringBuilder(client, account);

        Assertions.assertEquals(accountHistory, result);
    }

    public String stringBuilder(Client client, Account account) {

        String newLine = "\n";

        return new StringBuilder()
                .append("History of account n° " + account.getAccountNumber() + " : ")
                .append(newLine)
                .append(newLine)
                .append("Owner : " + client.getFistName() + " " + client.getLastName().toUpperCase())
                .append(newLine)
                .append("Creation date : " +account.getCreationDate())
                .append(newLine)
                .append("Closing date : " + (account.getClosingDate() != null ? account.getClosingDate() : "not closed"))
                .append(newLine)
                .append("Minimum balance authorized : " + account.getMinimumBalance())
                .append(newLine)
                .append("Current Balance : " + account.getBalance())
                .append(newLine)
                .append(newLine)
                .append("Transactions : ")
                .append(newLine)
                .append(account.getTransactions().stream().map(Transaction::toString).collect(Collectors.joining("\n")))
                .toString();
    }

}
