package com.exaltcompany.domain.port;

import com.exaltcompany.domain.exception.UnauthorizedTransaction;
import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.Transaction;

/**
 * WithdrawalService - Service which deals with withdrawal operations
 *
 * @author pyref 09/08/2022
 */
public interface WithdrawalService {

    /**
     *
     * @param clientId id of the client who withdraw
     * @param accountNumber id of the account to withdraw in
     * @param amount amount to withdraw
     * @return withdrawal processed
     * @throws UnknownRessourceException
     * @throws UnauthorizedTransaction
     */
    Transaction withdrawFromAccount(long clientId, long accountNumber, double amount) throws UnknownRessourceException, UnauthorizedTransaction;
}
