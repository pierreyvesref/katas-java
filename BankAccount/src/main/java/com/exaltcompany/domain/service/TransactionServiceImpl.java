package com.exaltcompany.domain.service;

import com.exaltcompany.domain.exception.UnauthorizedTransaction;
import com.exaltcompany.domain.exception.UnknownRessourceException;
import com.exaltcompany.domain.model.*;
import com.exaltcompany.domain.port.*;
import com.exaltcompany.domain.validator.TransactionValidator;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * AccountServiceImpl - Service which deals with all account operations
 *
 * @author pyref 08/08/2022
 */
@Service
public class TransactionServiceImpl implements DepositService, WithdrawalService {

    private ClientRepositoryPort clientRepositoryPort;

    private AccountRepositoryPort accountRepositoryPort;

    private TransactionRepositoryPort transactionRepositoryPort;

    private TransactionValidator transactionValidator;

    public TransactionServiceImpl(ClientRepositoryPort clientRepositoryPort, AccountRepositoryPort accountRepositoryPort,
                                  TransactionRepositoryPort transactionRepositoryPort, TransactionValidator transactionValidator) {
        this.clientRepositoryPort = clientRepositoryPort;
        this.accountRepositoryPort = accountRepositoryPort;
        this.transactionRepositoryPort = transactionRepositoryPort;
        this.transactionValidator = transactionValidator;
    }

    public Deposit depositInAccount(long clientId, long accountNumber, double amount) throws UnknownRessourceException, UnauthorizedTransaction {

        Client client = clientRepositoryPort.findClientById(clientId).orElseThrow(()-> new UnknownRessourceException("Unknown client"));
        Account account = accountRepositoryPort.findAccountByAccountNumber(accountNumber).orElseThrow(()-> new UnknownRessourceException("Unknown account"));

        double previousBalanceAmount = account.getBalance();
        double newBalanceAmount = account.addToBalance(amount);

        Deposit deposit = new Deposit(amount, LocalDateTime.now(), previousBalanceAmount, newBalanceAmount, account);

        validateTransaction(client, account, deposit);

        transactionRepositoryPort.saveTransaction(deposit);
        accountRepositoryPort.saveAccount(account);

        return deposit;
    }

    @Override
    public Withdrawal withdrawFromAccount(long clientId, long accountNumber, double amount) throws UnknownRessourceException, UnauthorizedTransaction {
        Client client = clientRepositoryPort.findClientById(clientId).orElseThrow(()-> new UnknownRessourceException("Unknown client"));
        Account account = accountRepositoryPort.findAccountByAccountNumber(accountNumber).orElseThrow(()-> new UnknownRessourceException("Unknown account"));

        double previousBalanceAmount = account.getBalance();
        double newBalanceAmount = account.substractToBalance(amount);

        Withdrawal withdrawal = new Withdrawal(amount, LocalDateTime.now(), previousBalanceAmount, newBalanceAmount, account);

        validateTransaction(client, account, withdrawal);
        validateFundCapacity(account, withdrawal);

        transactionRepositoryPort.saveTransaction(withdrawal);
        accountRepositoryPort.saveAccount(account);

        return withdrawal;
    }

    public void validateTransaction(Client client, Account account, Transaction transaction) throws UnauthorizedTransaction {
        if(client.getId() != account.getClient().getId()){
            throw new UnauthorizedTransaction("Client not owner of this account");
        }
        if(!transactionValidator.validateTransactionAmount(transaction.getAmount())){
            throw new UnauthorizedTransaction("Bad amount");
        }
        if(!transactionValidator.validateTransactionDate(account, transaction)){
            throw new UnauthorizedTransaction("Account is closed");
        }
    }

    public void validateFundCapacity(Account account, Transaction transaction) throws UnauthorizedTransaction {
        if(!transactionValidator.validateTransactionFundCapacity(account, transaction)){
            throw new UnauthorizedTransaction("Account has not enough funds");
        }
    }

}
