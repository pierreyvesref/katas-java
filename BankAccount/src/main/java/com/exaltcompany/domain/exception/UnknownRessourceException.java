package com.exaltcompany.domain.exception;

public class UnknownRessourceException extends Exception{
    public UnknownRessourceException(String message) {
        super(message);
    }
}
