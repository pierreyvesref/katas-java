package com.exaltcompany.domain.port;

import com.exaltcompany.domain.model.Client;

import java.util.List;

/**
 * ClientService - Service which deals with client operations
 *
 * @author pyref 09/08/2022
 */
public interface ClientService {

    List<Client> findAllClients();

    Client findClientById(long idClient);

}
