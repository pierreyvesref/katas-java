INSERT INTO client (last_name, first_name, birth_date, adress, postal_code)
VALUES ('Durand', 'Antoine', '1997-11-22', '3 rue de la Pompe', '75001');

INSERT INTO account (account_number, balance, creation_date, minimum_balance, client_id)
VALUES (12397237, 1000, '2020-01-01', -1000, 1);

INSERT INTO transaction (transaction_type, amount, transaction_date, previous_balance_amount, new_balance_amount, account_number_id)
VALUES ('D', 500, '2021-04-01 20:45:00', 1000, 1500, 12397237),
       ('W', 700, '2021-08-24 18:00:00', 1500, 800, 12397237),
       ('W', 50, '2022-02-15 12:34:00', 800, 750, 12397237),
       ('D', 250, '2022-07-01 08:13:00', 750, 1000, 12397237);