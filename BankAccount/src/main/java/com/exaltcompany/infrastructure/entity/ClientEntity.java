package com.exaltcompany.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Table(name = "CLIENT")
public class ClientEntity implements Serializable {

    private static final long serialVersionUID = 1238937L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String fistName;

    @Column(name = "birth_date", columnDefinition = "DATE", nullable = false)
    private LocalDate birthDate;

    @Column(name = "adress")
    private String adress;

    @Column(name = "postal_code")
    private String postalCode;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "client")
    private List<AccountEntity> accounts;

}
