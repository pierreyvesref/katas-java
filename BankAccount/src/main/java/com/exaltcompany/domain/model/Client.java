package com.exaltcompany.domain.model;

import java.time.LocalDate;
import java.util.List;

/**
 * Client
 *
 * @author pyref 08/08/2022
 */
public class Client {

    /* id - long, id of the client */
    private long id;

    /* lastName - String, lastName of the client */
    private String lastName;

    /* lastName - String, fistName of the client */
    private String fistName;

    /* birthdate - LocalDate, birthdate of the client */
    private LocalDate birthDate;

    /* adress - String, adress of the client */
    private String adress;

    /* postalCode - String, postalCode of the client */
    private String postalCode;

    /* accounts - List<Account>, accounts of the client */
    private List<Account> accounts;

    public Client(){}

    public Client(long id, String lastName, String fistName, LocalDate birthDate, String adress, String postalCode) {
        this.id = id;
        this.lastName = lastName;
        this.fistName = fistName;
        this.birthDate = birthDate;
        this.adress = adress;
        this.postalCode = postalCode;
    }

    public Client(long id, String lastName, String fistName, LocalDate birthDate, String adress, String postalCode, List<Account> accounts) {
        this.id = id;
        this.lastName = lastName;
        this.fistName = fistName;
        this.birthDate = birthDate;
        this.adress = adress;
        this.postalCode = postalCode;
        this.accounts = accounts;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFistName() {
        return fistName;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}
