package com.exaltcompany.domain.port;

import com.exaltcompany.domain.model.Account;

import java.util.Optional;

/**
 * AccountRepositoryPort - port for account persistence
 *
 * @author pyref 08/08/2022
 */
public interface AccountRepositoryPort {

    /**
     * Find an account by its account number
     *
     * @param accountNumber id of the account to find
     * @return Account object
     */
    Optional<Account> findAccountByAccountNumber(long accountNumber);

    /**
     * Save an account
     *
     * @param account account to save
     */
    void saveAccount(Account account);
}
