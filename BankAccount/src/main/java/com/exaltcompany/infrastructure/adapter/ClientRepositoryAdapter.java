package com.exaltcompany.infrastructure.adapter;

import com.exaltcompany.domain.model.Client;
import com.exaltcompany.domain.port.ClientRepositoryPort;
import com.exaltcompany.infrastructure.entity.ClientEntity;
import com.exaltcompany.infrastructure.map.EntityModelMapper;
import com.exaltcompany.infrastructure.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class ClientRepositoryAdapter implements ClientRepositoryPort {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private EntityModelMapper entityModelMapper;

    @Override
    public Optional<Client> findClientById(long id) {
        Optional<ClientEntity> clientEntity = clientRepository.findById(id);
        return Optional.of(entityModelMapper.mapClientEntityToClient(clientEntity.get()));
    }

    @Override
    public List<Client> findAllClients() {
        List<Client> clientList = new ArrayList<>();
        clientRepository.findAll().forEach(
                clientEntity -> clientList.add(entityModelMapper.mapClientEntityToClient(clientEntity))
        );

        return clientList;
    }
}
