package com.exalt.company.validator;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * Test class for NumberValidator
 *
 * @author pyref 07/08/2022
 */
class NumberValidatorTest {

    private NumberValidator numberValidator;

    @BeforeEach
    public void init(){
        numberValidator = new NumberValidator();
    }

    static Stream<Arguments> stringNumbersValid() {
        return Stream.of(
                Arguments.of("1", true),
                Arguments.of("24543532", true),
                Arguments.of("-423", true),
                Arguments.of("34.4", false),
                Arguments.of("", false),
                Arguments.of("a", false),
                Arguments.of("Ar3dks", false),
                Arguments.of("0-", false)
        );
    }

    @DisplayName("number validator test")
    @ParameterizedTest
    @MethodSource("stringNumbersValid")
    void isIntegerTest(String strNumber, boolean expected){
        boolean result = numberValidator.isInteger(strNumber);
        Assertions.assertThat(result).isEqualTo(expected);
    }

    static Stream<Arguments> stringPositiveNumbers() {
        return Stream.of(
                Arguments.of("1", true),
                Arguments.of("0", true),
                Arguments.of("2937", true),
                Arguments.of("-0", true),
                Arguments.of("-4", false),
                Arguments.of("-493", false)
        );
    }

    @DisplayName("positive validator test")
    @ParameterizedTest
    @MethodSource("stringPositiveNumbers")
    void isPositiveTest(String strNumber, boolean expected){
        boolean result = numberValidator.isPositive(strNumber);
        Assertions.assertThat(result).isEqualTo(expected);
    }

}
