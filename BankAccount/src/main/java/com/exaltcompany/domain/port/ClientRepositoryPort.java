package com.exaltcompany.domain.port;

import com.exaltcompany.domain.model.Client;

import java.util.List;
import java.util.Optional;

/**
 * ClientRepositoryPort - port for client persistence
 *
 * @author pyref 08/08/2022
 */
public interface ClientRepositoryPort {

    /**
     * Find a client by his id
     *
     * @param id id of the client to find
     * @return Client object
     */
    Optional<Client> findClientById(long id);

    /**
     * Find all the clients
     *
     * @return List<Client> object
     */
    List<Client> findAllClients();

}
